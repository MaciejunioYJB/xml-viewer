# XML Viewer

Aplikacja przeznaczona do przeglądania i edytowania danych znajdujących się w plikach XML.

# Instrukcja obsługi:
1. Po uruchomieniu aplikacji, użytkownik proszony jest (poprzez okno dialogowe) o wskazanie pliku XML, na którym będą wykonywane operacje.    

	![](https://bitbucket.org/MaciejunioYJB/xml-viewer/raw/a0d4dd8a2128975d13cb8c08094799dce19544c0/Images/open-file.PNG)  

2. Po wybraniu odpowiedniego pliku XML uruchomione jest okno główne aplikacji. W siatce znajdują się wszystkie wiersze wskazanego pliku XML. Użytkownik może je przeglądać. Szerokości kolumn są dopasowane do danych zawartych w widoku.

	![](https://bitbucket.org/MaciejunioYJB/xml-viewer/raw/4c8eea1d471830ab80c82a134991f6d2ba57a90b/Images/view-data.PNG)  
	
3. Jeśli użytkownik chce:
	+ Dodać nowy wiersz - wpisuje odpowiednie dane w pola tekstowe po prawej stronie ekranu i klika przycisk Add.
	+ Edytować istniejący wiersz - wybiera z widoku dany wiersz, jego dane pojawiają się w polach tekstowych i aby zatwierdzić, klika przycisk Edit.
	+ Usunąć istniejący wiersz - wybiera z widoku wiersz do usunięcia i klika przycisk Remove.  
    
4. ID jest unikalne. Dodanie użytkownika o istniejącym ID spowoduje wyświetlenie stosownego powiadomienia.

	![](https://bitbucket.org/MaciejunioYJB/xml-viewer/raw/4c8eea1d471830ab80c82a134991f6d2ba57a90b/Images/add-messagebox.PNG)  

5. Sortowanie odbywa się poprzez kliknięcie na wybraną kolumnę.

	![](https://bitbucket.org/MaciejunioYJB/xml-viewer/raw/4c8eea1d471830ab80c82a134991f6d2ba57a90b/Images/sort.PNG)	
	
6. Zmiany zapisywane są na bieżąco. Użytkownik w kazdej chwili może zamknąć aplikację i nie martwić się o zapis danych.  

# Struktura pliku XML wraz z odpowiadającymi typami danych w aplikacji:  
- ID (int)  
- FirstName (string)  
- LastName (string)  
- Birthdate (string)  
- Position (string)  
- Salary (double)  
- NIP (string)  

Contributor: Maciej Łaszkiewicz