﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Xml;
using System.Xml.Linq;

namespace MyDBapp
{
    public partial class MainWindow : Window
    {
        List<Employee> employees = new List<Employee>();
        string xml_path = "";

        public MainWindow()
        {
            InitializeComponent();

            xml_path = OpenFile();
            ReadXML(xml_path);
        }

        private string OpenFile()
        {
            string path = "";
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "XML Files (.xml)|*.xml|All files (*.*)|*.*";
            ofd.Title = "Open Your XML File";
            if (ofd.ShowDialog() == true)
            {
                string fullPath = ofd.FileName;
                path = fullPath;
            }
            
            return path;
        }

        private void ReadXML(string path)
        {
            try
            {
                var doc = XDocument.Load(path);
                employees = doc.Descendants("Employee").Select(node =>
                new Employee
                {
                    ID = Convert.ToInt32(node.Element("ID").Value),
                    FirstName = node.Element("FirstName").Value,
                    LastName = node.Element("LastName").Value,
                    Birthdate = node.Element("Birthdate").Value,
                    Position = node.Element("Position").Value,
                    Salary = Convert.ToDouble(node.Element("Salary").Value),
                    NIP = node.Element("NIP").Value
                }).ToList();
            }
            catch (Exception error)
            {
                MessageBox.Show("Catch exception: " + error.Message.ToString());
            }
            ViewXML(datagrid);
        }

        private void ViewXML(DataGrid viewer)
        {
            viewer.ItemsSource = null; //refresh datagrid
            viewer.ItemsSource = employees;
        }

        private void Add_Button_Click(object sender, RoutedEventArgs e)
        {
            int ID_ = Convert.ToInt32(ID_TextBox.Text);
            if (!employees.Exists(x => x.ID == ID_))
            {
                Employee employee = new Employee(ID_, FirstName_TextBox.Text, LastName_TextBox.Text,
                Birthdate_TextBox.Text, Position_TextBox.Text, Convert.ToDouble(Salary_TextBox.Text), NIP_TextBox.Text);

                employees.Add(employee);
                UpdateXML(employees);
                ViewXML(datagrid);
            }
            else MessageBox.Show("Employee with that ID already exist!");
        }

        private void UpdateXML(List<Employee> list)
        {
            using (XmlWriter writer = XmlWriter.Create(xml_path))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Employees");

                foreach (Employee employee in list)
                {
                    writer.WriteStartElement("Employee");

                    writer.WriteElementString("ID", employee.ID.ToString());
                    writer.WriteElementString("FirstName", employee.FirstName);
                    writer.WriteElementString("LastName", employee.LastName);
                    writer.WriteElementString("Birthdate", employee.Birthdate);
                    writer.WriteElementString("Position", employee.Position);
                    writer.WriteElementString("Salary", employee.Salary.ToString());
                    writer.WriteElementString("NIP", employee.NIP);

                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }

        private void Datagrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Employee x = new Employee();
            x = datagrid.SelectedItem as Employee;
            if (x != null)
            {
                //fill all textbox
                ID_TextBox.Text = x.ID.ToString();
                FirstName_TextBox.Text = x.FirstName;
                LastName_TextBox.Text = x.LastName;
                Birthdate_TextBox.Text = x.Birthdate;
                Position_TextBox.Text = x.Position;
                Salary_TextBox.Text = x.Salary.ToString();
                NIP_TextBox.Text = x.NIP;
            }
        }

        private void Edit_Button_Click(object sender, RoutedEventArgs e)
        {
            foreach(Employee x in employees)
            {
                if (x.ID == Convert.ToInt32(ID_TextBox.Text))
                {
                    x.FirstName = FirstName_TextBox.Text;
                    x.LastName = LastName_TextBox.Text;
                    x.Birthdate = Birthdate_TextBox.Text;
                    x.Position = Position_TextBox.Text;
                    x.Salary = Convert.ToDouble(Salary_TextBox.Text);
                    x.NIP = NIP_TextBox.Text;
                }
            }
            UpdateXML(employees);
            ViewXML(datagrid);
        }

        private void Remove_Button_Click(object sender, RoutedEventArgs e)
        {
            int ID_ = Convert.ToInt32(ID_TextBox.Text);
            Employee a = employees.Find(x => x.ID == ID_);
            employees.Remove(a);
            UpdateXML(employees);
            ViewXML(datagrid);
        }
    }
}