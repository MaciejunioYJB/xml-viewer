﻿namespace MyDBapp
{
    class Employee
    {
        private int id;
        private string firstname, lastname, birthdate, position, nip;
        private double salary;

        public Employee()
        {

        }

        public Employee(int id, string firstname, string lastname, string birthdate, string position, double salary, string NIP)
        {
            this.id = id;
            this.firstname = firstname;
            this.lastname = lastname;
            this.birthdate = birthdate;
            this.position = position;
            this.salary = salary;
            this.nip = NIP;
        }

        public int ID
        {
            get { return id; }
            set { id = value; }
        }
        public string FirstName
        {
            get { return firstname; }
            set { firstname = value; }
        }
        public string LastName
        {
            get { return lastname; }
            set { lastname = value; }
        }
        public string Birthdate
        {
            get { return birthdate; }
            set { birthdate = value; }
        }
        public string Position
        {
            get { return position; }
            set { position = value; }
        }
        public double Salary
        {
            get { return salary; }
            set { salary = value; }
        }
        public string NIP
        {
            get { return nip; }
            set { nip = value; }
        }
    }
}
